function calculator() {
    let price = Number(document.getElementById("price").value);
    let quantity = Number(document.getElementById("quantity").value);
    let result = price * quantity;

    if (Number.isNaN(result)) {
        let line = "Ошибка! Введите подходящие значения";
        document.getElementById("result").innerHTML = line;
    } else {
        document.getElementById("result").innerHTML = result;
    }
    if(result<0){
        let line = "Ошибка! Введите подходящие значения";
        document.getElementById("result").innerHTML = line;
    }
}

window.addEventListener("DOMContentLoaded", function () {
    let b = document.getElementById("getCount");
    b.addEventListener("click", calculator);
});